﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using System;

public class SoundManager : MonoBehaviour {
    private static SoundManager instance;
    public static SoundManager Instance { get { return instance; } }

    public Sound[] sounds;


    private void Awake()
    {
        instance = this;
        foreach(Sound s in sounds)
        {
            s.source = gameObject.AddComponent<AudioSource>();

            s.source.clip = s.clip;
            s.source.pitch = s.pitch;
            s.source.volume = s.volume;
        }
    }

    public void Play(string name)
    {
        Sound s = Array.Find(sounds, sound => sound.name == name);
        s.source.Play();
    }

    

    public AudioClip GetClip(string name)
    {
        var audio = Array.Find(sounds, sound => sound.name == name);
        return audio == null ? null : audio.clip;
    }
}
