﻿using System.Collections.Generic;
using UnityEngine;

public class Flock : MonoBehaviour {
    public static float LeftControlLock = 0f;
    public static float RightControlLock = 0f;
    public const float ControlLockDefault = 0.2f;

    public float moveSpeed = 3.0f;
    public float xSpeed = 2.0f;
    public List<Pigeon> pigeons;

    private Formation formation;

    public Formation Formation { get { return formation; } }

	void Awake () {
        formation = Formation.Undefined;
    }

    void Update()
    {
        LeftControlLock -= Time.deltaTime;
        if(LeftControlLock < 0f) {
            LeftControlLock = 0f;
        }
        RightControlLock -= Time.deltaTime;
        if(RightControlLock < 0f) {
            RightControlLock = 0f;
        }

        var v = transform.position;
        v.z += moveSpeed* Time.deltaTime;
        transform.position = v;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="dir"> dir is [-1.0f; 1.0f]</param>
    public void MoveX(float dir)
    {
        if(dir > 0f && LeftControlLock > 0f || dir < 0f && RightControlLock > 0f)
        {
            return;
        }

        var v = transform.position;
        v.x += xSpeed * Time.deltaTime * dir;
        
        //if (v.x < GameMap.Instance.LeftLimit && )
        //{
        //    v.x = GameMap.Instance.LeftLimit;
        //}
        //else if (v.x > GameMap.Instance.RightLimit && RightControlLock <= 0f)
        //{
        //    v.x = GameMap.Instance.RightLimit;
        //}
        transform.position = v;
    }

    public void ChangeFormation(Formation formation)
    {
        //if(this.formation == formation && !formationInProgress)
        //{
        //    return;
        //}
        bool formationChanged = false;
        switch(formation)
        {
            case Formation.Single:
                formationChanged = FormationToSingle();
                break;
            case Formation.Double:
                formationChanged = FormationToDouble();
                break;
            case Formation.Triple:
                formationChanged = FormationToTriple();
                break;
            case Formation.Column:
                formationChanged = FormationToColumn();
                break;
        }
        if (formationChanged)
        {
            this.formation = formation;
        }
    }

    private void FormateToRect(Vector2 areaMin, Vector2 areaMax, List<Pigeon> pigeons)
    {
        int rows = Mathf.RoundToInt(Mathf.Sqrt(pigeons.Count));
        int placed = 0;
        int total = pigeons.Count;

        var avSizes = CalculateAverageSizes(pigeons);

        float startY = -3f -avSizes.y * rows;//areaMin.y + ((areaMax.y - areaMin.y) - avSizes.y * rows) / 2;

        for (int i = 0; i < rows; ++i)
        {
            int placeCount = (i + 1 != rows) ? (pigeons.Count / rows) : (total);
            float startX = areaMin.x + ((areaMax.x - areaMin.x) - avSizes.x * placeCount) / 2;
            //place pigeons in a row
            for (int j = 0; j < placeCount; ++j)
            {
                var pos = pigeons[placed + j].transform.localPosition;
                pos.x = startX + j * avSizes.x + avSizes.x / 2;
                pos.z = startY + i * avSizes.y + avSizes.y / 2;
                if(j % 2 == 0) {
                    pos.z += avSizes.y / 3;
                } 
                pigeons[placed + j].SetTarget(pos);
            }
            placed += placeCount;
            total -= placeCount;
        }

        LeftControlLock = 0f;
        RightControlLock = 0f;
    }

    Vector2 CalculateAverageSizes(List<Pigeon> pigeons)
    {
        float averageYSize = 0, averageXSize = 0;
        for (int i = 0; i < pigeons.Count; ++i)
        {
            averageYSize += pigeons[i].MaxBounds.y - pigeons[i].MinBounds.y;
            averageXSize += pigeons[i].MaxBounds.x - pigeons[i].MinBounds.x;
        }
        averageYSize /= pigeons.Count;
        averageXSize /= pigeons.Count;
        return new Vector2(averageXSize, averageYSize);
    }

    static Vector3[] GetPositions(List<Pigeon> pigeons)
    {
        Vector3[] positions = new Vector3[pigeons.Count];
        for(int i = 0; i < pigeons.Count; ++i)
        {
            positions[i] = pigeons[i].transform.position;
        }
        return positions;
    }

    static void SetPositions(ref List<Pigeon> pigeons, Vector3[] positions) {
        for(int i = 0; i < pigeons.Count; ++i) {
            pigeons[i].transform.position = positions[i];
        }
    }

    private bool FormationToSingle()
    {
        var avSizes = CalculateAverageSizes(pigeons);
        int rows = Mathf.RoundToInt(Mathf.Sqrt(pigeons.Count));
        float SPACE = avSizes.y * rows;
        var positions = GetPositions(pigeons);
        transform.localPosition = new Vector3(0f, transform.localPosition.y, transform.localPosition.z);
        SetPositions(ref pigeons, positions);
        Vector2 areaMin = new Vector2(transform.position.x - SPACE, transform.position.z - SPACE);
        Vector2 areaMax = new Vector2(transform.position.x + SPACE, transform.position.z);

        FormateToRect(areaMin, areaMax, pigeons);
        return true;
    }

    private bool FormationToDouble()
    {
        if(pigeons.Count < 2)
        {
            return false;
        }
        var avSizes = CalculateAverageSizes(pigeons);
        int rows = Mathf.RoundToInt(Mathf.Sqrt(pigeons.Count / 2));
        float SPACE = 2f;// avSizes.x / 2 * rows;

        var positions = GetPositions(pigeons);
        transform.localPosition = new Vector3(0f, transform.localPosition.y, transform.localPosition.z);
        SetPositions(ref pigeons, positions);

        Vector2 areaMin = new Vector2(transform.position.x - SPACE * 3.5f, transform.position.z - SPACE);
        Vector2 areaMax = new Vector2(transform.position.x - SPACE * 1.5f, transform.position.z );

        var pigeons1 = pigeons.GetRange(0, pigeons.Count / 2);
        var pigeons2 = pigeons.GetRange(pigeons.Count / 2, pigeons.Count - pigeons.Count / 2);
        FormateToRect(areaMin, areaMax, pigeons1);
        areaMin = new Vector2(transform.position.x + SPACE * 1.5f, transform.position.z - SPACE * 2);
        areaMax = new Vector2(transform.position.x + SPACE * 3.5f, transform.position.z );
        FormateToRect(areaMin, areaMax, pigeons2);
        return true;
    }

    private bool FormationToTriple()
    {
        if(pigeons.Count < 3)
        {
            return false;
        }

        var avSizes = CalculateAverageSizes(pigeons);
        int rows = Mathf.RoundToInt(Mathf.Sqrt(pigeons.Count / 3));
        float SPACE = 2.75f;

        var positions = GetPositions(pigeons);
        transform.localPosition = new Vector3(0f, transform.localPosition.y, transform.localPosition.z);
        SetPositions(ref pigeons, positions);

        Vector2 areaMin = new Vector2(transform.position.x - SPACE * 3.5f, transform.position.z - SPACE);
        Vector2 areaMax = new Vector2(transform.position.x - SPACE * 1.5f, transform.position.z);

        var pigeons1 = pigeons.GetRange(0, pigeons.Count / 3);
        var pigeons2 = pigeons.GetRange(pigeons.Count / 3, pigeons.Count / 3);
        var pigeons3 = pigeons.GetRange(pigeons.Count / 3 + pigeons.Count / 3, pigeons.Count - pigeons.Count / 3 - pigeons.Count / 3);
        FormateToRect(areaMin, areaMax, pigeons1);
        areaMin = new Vector2(transform.position.x - SPACE, transform.position.z - SPACE * 2);
        areaMax = new Vector2(transform.position.x + SPACE, transform.position.z);
        FormateToRect(areaMin, areaMax, pigeons2);
        areaMin = new Vector2(transform.position.x + SPACE * 1.5f, transform.position.z - SPACE * 2);
        areaMax = new Vector2(transform.position.x + SPACE * 3.5f, transform.position.z);
        FormateToRect(areaMin, areaMax, pigeons3);

        return true;
    }

    private bool FormationToColumn() {

        return true;
    }

    public void PigeonKilled(Pigeon pigeon) {
        pigeons.Remove(pigeon);
        Destroy(pigeon.gameObject);
    }
}

public enum Formation
{
    Undefined = 0,
    Single,
    Double,
    Triple,
    Column
}
