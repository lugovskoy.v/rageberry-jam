﻿using UnityEngine;

public class Hazard : MonoBehaviour {
    public float zOffset = 0;
    public float height;
    public float freeSpaceRequired = 1.5f;

    protected AudioSource audioSource;

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(transform.position, new Vector3(20f, 2f, height));
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireCube(transform.position + new Vector3(0, 0, height / 2 + freeSpaceRequired / 2), new Vector3(20f, 2f, freeSpaceRequired));
    }

    protected void ControlAudioByZ()
    {
        const float maxDist = 20f;
        const float minDist = -15f;
        const float minVolume = 0.01f;
        const float maxVolume = 0.35f;
        float z = GameController.GetFlockPosZ();
        float dist = zOffset - z;
        if (dist >= minDist && dist < 0)
        {
            audioSource.priority = 250;
            audioSource.volume = (1f + dist / minDist) * (maxVolume - minVolume) + minVolume;
        } else if ( dist >=0 && dist < maxDist) {
            audioSource.priority = 220;
            audioSource.volume = (1f - dist / maxDist) * (maxVolume - minVolume) + minVolume;
        } else
        {
            audioSource.volume = 0f;
        }
    }
}
