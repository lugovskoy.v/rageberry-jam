﻿using System.Collections.Generic;
using UnityEngine;

public class GameMap : MonoBehaviour {

    private static GameMap instance;
    public static GameMap Instance { get { return instance; } }

    [Header("Map data")]
    public List<Hazard> hazards;

    public float mapWidth = 8f;

    [SerializeField]
    private int length;
    
    public int Length { get { return length; } }
    public float LeftLimit { get { return  - mapWidth / 2f; } }
    public float RightLimit { get { return mapWidth / 2f; } }

    private void Awake()
    {
        instance = this;
    }
}
