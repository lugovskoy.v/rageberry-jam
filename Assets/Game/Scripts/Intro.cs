﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

using UnityEngine.SceneManagement;

public class Intro : MonoBehaviour {

    public VideoPlayer vPlayer;

    void Awake () {
        vPlayer.loopPointReached += EndReached;
    }

    void EndReached(UnityEngine.Video.VideoPlayer vp) {
        SceneManager.LoadScene("menu");
    }

    void Update() {
        if(Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.Space)) {
            SceneManager.LoadScene("menu");
        }
    }
}
