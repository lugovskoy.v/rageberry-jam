﻿using UnityEngine;

public class Chainsaw : Hazard {
    public const float rotationSpeed = 540f;
    public Transform spinnableSaw;


    static readonly string[] DeathSounds = { "DeathBySaw_01", "DeathBySaw_02", "DeathBySaw_03", "DeathBySaw_04" };

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
        audioSource.volume = 0f;
        var angles = spinnableSaw.localRotation.eulerAngles;
        angles.z = Random.Range(0f, 360f);
        spinnableSaw.localRotation = Quaternion.Euler(angles);
        
    }

    private void Update()
    {
        spinnableSaw.Rotate(new Vector3(0, 0, 1f), rotationSpeed * Time.deltaTime, Space.Self);
        ControlAudioByZ();
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Pigeon") {
            GameController.KillPigeon(other.GetComponent<Pigeon>());
            var clip = SoundManager.Instance.GetClip(DeathSounds[Random.Range(0, DeathSounds.Length)]);
            GameController.Instance.audioCamera.PlayOneShot(clip);
            //audioSource.PlayOneShot(SoundManager.Instance.GetClip("Saw_01"));
        }
    }

}
