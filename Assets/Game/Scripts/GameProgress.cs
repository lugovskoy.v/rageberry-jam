﻿
public static class GameProgress {
    private static int curLevel = 1;
    public static int CurrentLevel { get { return curLevel; } }

    /// <summary>
    /// 
    /// </summary>
    /// <returns>True if game finished</returns>
    public static bool LevelFinished()
    {
        ++curLevel;
        if(curLevel > 3)
        {
            curLevel = 0;
            return true;
        }
        return false;
    }
	
}
