﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ExplosivePipe : Hazard {

    public float explosionRadius = 4f;
    public bool isExploded = false;
    protected Animator animator;
    public ParticleSystem vfxExplosion;

    static readonly string[] DeathSounds = { "DeathByGas_01", "DeathByGas_02" };

    void Awake() {
        audioSource = GetComponent<AudioSource>();
        animator = GetComponentInChildren<Animator>();
    }

    void Update() {
        if(!isExploded && GameController.GetFlockPosZ() > zOffset) {
            isExploded = true;
            vfxExplosion.Play();
            Destroy(vfxExplosion.gameObject, 3f);
            StartCoroutine(PlaySoundWithDelay(SoundManager.Instance.GetClip("GasExplosion_01"), 0.75f));
            //audioSource.PlayOneShot(SoundManager.Instance.GetClip("GasExplosion_01"));
            animator.SetBool("Exploded", isExploded);
        }
    }

    IEnumerator PlaySoundWithDelay(AudioClip clip, float delay)
    {
        yield return new WaitForSeconds(delay);
        audioSource.PlayOneShot(clip);
    }

    void OnTriggerExit(Collider other) {
        if (isExploded && other.tag == "Pigeon") {
            GameController.KillPigeon(other.GetComponent<Pigeon>());
            audioSource.PlayOneShot(SoundManager.Instance.GetClip(DeathSounds[Random.Range(0, DeathSounds.Length)]));
        }
    }
}
