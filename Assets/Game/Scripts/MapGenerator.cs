﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class MapGenerator {

    private static List<Hazard> hazardPrefabs;
    private static Transform trMap;

    public static Hazard InstantiateRandomHazard(float zOffset) {
        return Object.Instantiate(hazardPrefabs[Random.Range(0, hazardPrefabs.Count)], new Vector3(0f, 0f, zOffset), Quaternion.identity, trMap);
    }


    public static void Generate(List<Hazard> hazards, ref GameMap map) {
        hazardPrefabs = hazards;
        trMap = map.transform;

        const float START_OFFSET = 20f;
        const float FINISH_OFFSET = 5f;

        float zOfsset = START_OFFSET;
        
        float limitLength = map.Length - FINISH_OFFSET;
        while (zOfsset < limitLength)
        {
            var hazard = InstantiateRandomHazard(zOfsset);
            var childrenHazards = hazard.GetComponentsInChildren<Hazard>();
            for(int i = 0; i < childrenHazards.Length; ++i)
            {
                childrenHazards[i].zOffset = zOfsset;
            }
            hazard.zOffset = zOfsset;
            map.hazards.Add(hazard);
            zOfsset += hazard.freeSpaceRequired + hazard.height;
        }
        hazardPrefabs = null;
        trMap = null;

    }
    
}
