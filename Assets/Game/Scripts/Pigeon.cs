﻿using UnityEngine;

public class Pigeon : MonoBehaviour {
    
    private CapsuleCollider colPigeon;
    private Animator animator;

    public Vector2 MaxBounds { get { var p = colPigeon.bounds.max; return new Vector2(p.x, p.z); } }
    public Vector2 MinBounds { get { var p = colPigeon.bounds.min; return new Vector2(p.x, p.z); } }

    private Vector3 localTarget;
    private Vector3 curVelocity;
    private float curTargetTime;
    private const float TargetTime = 0.33f;
    public void SetTarget(Vector3 target) {
        localTarget = target;
        curTargetTime = TargetTime;
    }

    private void Awake()
    {
        colPigeon = GetComponent<CapsuleCollider>();
        animator = GetComponentInChildren<Animator>();
        var randomIdleStart = Random.Range(0, animator.GetCurrentAnimatorStateInfo(0).length); //Set a random part of the animation to start from
        animator.Play("Fly", 0, randomIdleStart);
    }

    private void Update()
    {
        if(localTarget == transform.localPosition) {
            curVelocity = Vector3.zero;
        } else {
            transform.localPosition = Vector3.SmoothDamp(transform.localPosition, localTarget, ref curVelocity, curTargetTime);
            curTargetTime -= Time.deltaTime;
            if(curTargetTime <= 0f) {
                transform.localPosition = localTarget;
            }
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if(other.tag == "WallLeft") {
            Flock.LeftControlLock = Flock.ControlLockDefault;
        } else if (other.tag == "WallRight") {
            Flock.RightControlLock = Flock.ControlLockDefault;
        }
    }
}
