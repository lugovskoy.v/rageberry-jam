﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GasPipe : Hazard {

    static readonly string[] DeathSounds = { "DeathByGas_01", "DeathByGas_02" };

    void Awake() {
        audioSource = GetComponent<AudioSource>();
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Pigeon")
        {
            GameController.KillPigeon(other.GetComponent<Pigeon>());
            audioSource.PlayOneShot(SoundManager.Instance.GetClip(DeathSounds[Random.Range(0, DeathSounds.Length)]));
        }
    }
}
