﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour {
    private static GameController instance;
    public static GameController Instance { get { return instance; } }

    public List<Hazard> hazardPrefabs;
    public Transform trCamera;
    public GameMap map;
    public Flock flock;
    public Pigeon pigeonPrefab;
    public AudioSource audioCamera;
    public int initialPegions = 16;

    public GameObject goVictory, goDefeat;

    private void Awake() {
        instance = this;

        MapGenerator.Generate(hazardPrefabs, ref map);

        for (int i = 0; i < initialPegions; ++i) {
            flock.pigeons.Add(Instantiate(pigeonPrefab, flock.transform));
        }
        flock.ChangeFormation(Formation.Single);

        flock.moveSpeed = 4f + (GameProgress.CurrentLevel - 1) * 2f;
    }

    bool VictoryScreenShown = false;

    void Update () {
        if(Input.GetKeyDown(KeyCode.Alpha1)) {
            flock.ChangeFormation(Formation.Single);
        } if (Input.GetKeyDown(KeyCode.Alpha2)) {
            flock.ChangeFormation(Formation.Double);
        } else if (Input.GetKeyDown(KeyCode.Alpha3)) {
            flock.ChangeFormation(Formation.Triple);
        } else if (Input.GetKeyDown(KeyCode.Alpha4)) {
            flock.ChangeFormation(Formation.Column);
        }

        if(Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A)) {
            flock.MoveX(1f);
        }
        if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D)) {
            flock.MoveX(-1f);
        }

        var camPos = trCamera.transform.position;
        if(camPos.z < map.Length) {
            camPos.z += flock.moveSpeed * Time.deltaTime;
            trCamera.transform.position = camPos;
        } else if(!VictoryScreenShown) {
            VictoryScreenShown = true;
            if(GameProgress.LevelFinished()) {
                //Show ending screen
                SceneManager.LoadScene("final");
            } else {
                //Show victory screen
                goVictory.SetActive(true);
            }
        }

        if(flock.pigeons.Count == 0 && !VictoryScreenShown) {
            VictoryScreenShown = true;
            //Show defeat screen
            goDefeat.SetActive(true);
        }

    }

    public static void KillPigeon(Pigeon pigeon) {
        Instance.flock.PigeonKilled(pigeon);
    }

    public static float GetFlockPosZ() {
        return Instance.flock.transform.position.z;
    }

    private void OnDestroy()
    {
        instance = null;
        
        flock = null;
    }

    public void GoToMainMenu()
    {
        SceneManager.LoadScene("menu");
    }

    public void NextLevel()
    {
        SceneManager.LoadScene("main");
    }
}
